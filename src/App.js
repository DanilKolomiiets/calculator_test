import './App.css';
import CalculatorEngine from './components/CalculatorEngine'

function App() {
  return (
    <CalculatorEngine />
  );
}

export default App;
