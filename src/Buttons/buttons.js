
export const allButtons = [
    {id: "AC", symbol: "AC", className: 'default-buttons'},
    {id: "delete", symbol: "del", className: 'default-buttons'},
    {id: "ostatok", symbol: "%", className: 'default-buttons'},
    {id: "division", symbol: "/", className: 'activity-buttons'},
    {id: "7", symbol: 7, className: 'default-buttons'},
    {id: "8", symbol: 8, className: 'default-buttons'},
    {id: "9", symbol: 9, className: 'default-buttons'},
    {id: "multiplication", symbol: '*', className: 'activity-buttons'},
    {id: "4", symbol: 4, className: 'default-buttons'},
    {id: "5", symbol: 5, className: 'default-buttons'},
    {id: "6", symbol: 6, className: 'default-buttons'},
    {id: "substraction", symbol: '-', className: 'activity-buttons'},
    {id: "1", symbol: 1, className: 'default-buttons'},
    {id: "2", symbol: 2, className: 'default-buttons'},
    {id: "3", symbol: 3, className: 'default-buttons'},
    {id: "addition", symbol: '+', className: 'activity-buttons'},
    {id: "dot", symbol: ".", className: 'default-buttons'},
    {id: "0", symbol: 0, className: 'default-buttons'},
    {id: "equal", symbol: '=', className: 'activity-buttons'}    
]
