 import './CreateButtons.css'
 import React from 'react'; 


const CreateButtons = ({buttons, action})=>{

     return (
         <div className='main-area'>
             {buttons.map(item => (
                    <button 
                            className= {item.className}
                            key={item.id}
                            id={item.symbol}
                            onClick={
                                (e) =>{
                                    e.preventDefault();
                                    action(item.symbol)
                                } }>
                        {item.symbol}
                    </button>
             ))}
         </div>
       );
}

 export default CreateButtons;