import Display from "./Display";
import CreateButtons from './CreateButtons'
import {allButtons} from '../Buttons/buttons'
import React, { useState, useEffect } from 'react'; 

function CalculatorEngine() {
    const [inputValue, setInputValue] = useState('')
    const [result, setResult] = useState('')
    const [lastChildAttention, setLastChildAttention] = useState(true)
    const [canShowRes, setCanShowRes] = useState(false)

    useEffect(() => {
    if(canShowRes){
        showResult(inputValue)
      }
    });

    const addInputValue = (symbol) =>{
        if(symbol === '='){
          const primer = parseData(inputValue)
          setInputValue(primer)
          setCanShowRes(false)
          setResult('')
        }else if(symbol === 'AC'){
          setInputValue('')
          setResult('')
        }else if (symbol === '+' || symbol === '-' || symbol === '*' || symbol === '/'){
          setLastChildAttention(false)
          setCanShowRes(true)
          setInputValue(
          inputValue + symbol
          )  
        }else{
          setLastChildAttention(true)
          setInputValue(
            inputValue + symbol
          )       
        } 
            
    }        
    
    const showResult = (data) => {
      if(lastChildAttention){
        const result = parseData(data)
          setResult(result)
      }
    }

    function parseData(fn) {
      return new Function('return ' + fn)();
    }

    const getID = (e) => {
      addInputValue(e)
    }

    const IsItActivitySymbol = (symbol)=>{
      if (symbol === '+' || symbol === '-' || symbol === '*' || symbol === '/'){
        setLastChildAttention(false);
        setCanShowRes(true);
      }else{setLastChildAttention(true)}
    }



    const filterForKeyBoardInput = (symbol) => {
      if (symbol === '+' || symbol === '-' || symbol === '*' || symbol === '/'){
        addInputValue(symbol)
      }else if(Number(symbol) || symbol === '0'){
        addInputValue(symbol)
      }else{
        addInputValue('')
      }
    }

    const listenKeyboard = (e)=> {
      const cutInput = e.split('')
      setLastChildAttention(true)
      cutInput.forEach(item => {
        IsItActivitySymbol(item)
      })
      cutInput.forEach(item => {
        filterForKeyBoardInput(item)
      })
      
      
  }

  return (
    <div className="App">
      <div className="calc-wrapper">
        <div className="working-panel">
            <Display inputValue = {inputValue} result = {result} listener = {listenKeyboard}/>
            <div className="buttons-area">
                <CreateButtons buttons={allButtons} action={getID}/>
            </div>
        </div>
      </div>
    </div>
  );
}

export default CalculatorEngine;
