import React from 'react';
import './Display.css'

const Display = ({inputValue, result, listener})=>{
  return (
      <div className='display-area'>
         <input
          value={inputValue} 
          onChange={
            (e) =>{
              listener(e.target.value)
            }
          }
        />
        <input
          id='result'
          value={result}
          read
        />
      </div>
     
  );
}


export default Display;